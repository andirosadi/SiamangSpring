@extends('layouts.master')

@section('title')
  Siamang Shop
@endsection

@section('content')
  <div class="row">
    <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
      <h1>Checkout</h1>
      <h4>Total belanja anda: Rp {{ $total }}</h4>
      <form action="{{route('checkout')}}" method="post">
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <label for="name">Nama</label>
              <input id="name" type="text" class="form-control" requeired/>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="name">Alamat</label>
              <input id="address" type="text" class="form-control" requeired/>
            </div>
          </div>
          <hr>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="name">A.n Kartu Credit</label>
              <input id="card-name" type="text" class="form-control" requeired/>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="name">Nomer Kartu Credit</label>
              <input id="card-number" type="text" class="form-control" requeired/>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="name">Bulan Kadaluarsa</label>
              <input id="card-expiry-month" type="text" class="form-control" requeired/>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="name">Tahun Kadaluarsa</label>
              <input id="card-expiry-year" type="text" class="form-control" requeired/>
            </div>
          </div>
        <div class="col-xs-12">
          <div class="form-group">
            <label for="name">CVC</label>
            <input id="card-cvc" type="text" class="form-control" requeired/>
          </div>
        </div>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success">Beli sekarang!</button>
      </form>
  </div>
  </div>
@endsection
