package com.keldua.siamangspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiamangSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiamangSpringApplication.class, args);
	}
}
