package com.keldua.siamangspring.repository;

import org.springframework.data.repository.CrudRepository;

import com.keldua.siamangspring.model.Product;

public interface ProductRepository extends CrudRepository<Product, String> {



}

