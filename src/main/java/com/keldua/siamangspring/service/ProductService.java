package com.keldua.siamangspring.service;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.keldua.siamangspring.controller.ProductController;
import com.keldua.siamangspring.model.Product;
import com.keldua.siamangspring.repository.ProductRepository;

@Service
public class ProductService {
  @Autowired
  private ProductRepository productRepository;


  private List<Product> products =
      new ArrayList<>(
          Arrays.asList(
              new Product(
                  1,
                  "Historical Romance: The Earl's Mistaken Bride (Mempelai Sang Earl)",
                  "https://cdn.gramedia.com/uploads/items/9786020382340_Historical-Ro__w200_hauto.jpg",
                  70000),
              new Product(
                  2,
                  "ChickLit: Katwalk",
                  "https://cdn.gramedia.com/uploads/items/9786020378275_ChickLit-Katw__w200_hauto.jpg",
                  85000),
              new Product(
                  3,
                  "Dragon Ball Vol. 29",
                  "https://cdn.gramedia.com/uploads/items/9786020459776_Dragon_Ball_29__w200_hauto.jpg",
                  25000)));

  public List<Product> getAllProducts() {
    //return products;
    List<Product> products = new ArrayList<>();
    productRepository.findAll().forEach(products::add);

    return products;
  }

  public Product getProduct(int id) {
    return products.stream().filter(p -> p.getId() == id).findFirst().get();
    //return products;
  }


  public void addProduct(Product product) {
      //products.add(product);
      productRepository.save(product);

    }

    public void updateProduct(int id, Product product) {
    for (int i = 0; i < products.size(); i++) {
      Product p = products.get(i);
      if (p.getId() == id) {
        products.set(i, product);
        return;
      }
    }
  }

      public void deleteProduct(int id) {
        products.removeIf(p -> p.getId() == id);
  }
}

